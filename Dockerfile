### First stage ###
FROM golang:1.15-alpine AS builder

COPY . /snippet01

WORKDIR /snippet01

RUN go mod download

ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64
RUN go build /snippet01/cmd/web

### Second Stage ###
FROM alpine:latest

COPY --from=builder /snippet01 .

EXPOSE 4000

CMD ["./web"]

